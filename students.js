// 0. Вводные данные.
var studentsAndPoints = [
      'Алексей Петров', 0,
      'Ирина Овчинникова', 60,
      'Глеб Стукалов', 30,
      'Антон Павлович', 30,
      'Виктория Заровская', 30,
      'Алексей Левенец', 70,
      'Тимур Вамуш', 30,
      'Евгений Прочан', 60,
      'Александр Малов', 0,
    ],
    newStudents = [
      'Николай Фролов',
      'Олег Боровой'
    ],
    newPoints = [
      'Антон Павлович', 10,
      'Николай Фролов', 10,
    ];

// 1. Вывести список студентов. 
console.log('1. Список студентов:');

for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
  console.log('Студент %s набрал %d баллов', studentsAndPoints[i], studentsAndPoints[i + 1]);
}

// 2. Найти студента, набравшего наибольшее кол-во баллов.

var bestStudent,
    pointsMax = 0;

console.log('\n2. Студент, набравший максимальный балл:');

for (i = 1, imax = studentsAndPoints.length; i < imax; i += 2) {
  if (pointsMax === undefined || pointsMax < studentsAndPoints[i]) {
    pointsMax = studentsAndPoints[i];
    bestStudent = studentsAndPoints[i - 1];
  }
}

console.log('Студент %s имеет максимальный бал %d', bestStudent, pointsMax);

// 3. Добавить новичков.

for (i = 0, imax = newStudents.length; i < imax; ++i) {
  studentsAndPoints.push(newStudents[i], 0);
}

console.log('\n3. Обновленный список студентов:');

for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
  console.log('Студент %s набрал %d баллов', studentsAndPoints[i], studentsAndPoints[i + 1]);
}

// 4. Добавить баллы Антону Павловичу и Николаю Фролову.

var j;

for (i = 0, imax = newPoints.length; i < imax; i += 2) {
  j = studentsAndPoints.indexOf(newPoints[i]);
  if (j !== -1) {
    studentsAndPoints[j + 1] += newPoints[i + 1];
  } else {
    studentsAndPoints.push(newPoints[i], newPoints[i + 1]);
  }
}

console.log('\n4. Обновлена информация о баллах:');

for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
  console.log('Студент %s набрал %d баллов', studentsAndPoints[i], studentsAndPoints[i + 1]);
}

// 5. Вывести список студентов, не набравших баллы.

var points0 = [];

for (i = 1, imax = studentsAndPoints.length; i < imax; i += 2) {
  if (studentsAndPoints[i] === 0) {
    points0.push(studentsAndPoints[i - 1]);
  }
}

console.log('\n5. Студенты, не набравшие баллы:');

for (i = 0, imax = points0.length; i < imax; ++i) {
 console.log(points0[i]); 
}

// 6. Удалить данные студентов, набравших 0 баллов.

// Вариант с использованием массива, созданного в предыдущем задании
//for (i = 0, imax = points0.length; i < imax; ++i) {
//  studentsAndPoints.splice(studentsAndPoints.indexOf(points0[i]), 2);
//}
//
//console.log('\n6. Студенты, набравшие баллы:');
//
//for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
//  console.log('Студент %s набрал %d баллов', studentsAndPoints[i], studentsAndPoints[i + 1]);
//}

// Вариант без дополнительного массива

for (i = 1, imax = studentsAndPoints.length; i < imax; i += 2) {
  if (studentsAndPoints[i] === 0) {
    studentsAndPoints.splice(i - 1, 2);
  }
}

console.log('\n6. Студенты, набравшие баллы:');

for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
  console.log('Студент %s набрал %d баллов', studentsAndPoints[i], studentsAndPoints[i + 1]);
}